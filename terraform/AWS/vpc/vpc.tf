provider "aws" {
  profile = "default"
  region  = "us-west-2"
  shared_credentials_file = "~/.aws"
  #access_key = var.access_key
  #secret_key = var.secret_key
}

locals {
  name   = "devopslab-vpc"
  region = "us-west-2"
  tags = {
    Terraform = "true"
    Environment = "devopslab"
  }
}

resource "aws_eip" "nat" {
  vpc = true
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"
  name = local.name
  cidr = "10.10.0.0/16" # 10.0.0.0/8 is reserved for EC2-Classic

  azs             = ["${local.region}a", "${local.region}b"]
  private_subnets = ["${var.subnet_prv1}", "${var.subnet_prv2}"]
  public_subnets  = ["${var.subnet_pub1}", "${var.subnet_pub2}"]

  enable_nat_gateway = true
  reuse_nat_ips       = true
  single_nat_gateway  = true
  external_nat_ip_ids = "${aws_eip.nat.*.id}"
  #enable_vpn_gateway = true

  create_database_subnet_group = false

  manage_default_network_acl = true
  default_network_acl_tags   = { Name = "${local.name}-default" }

  manage_default_route_table = true
  default_route_table_tags   = { Name = "${local.name}-default" }

  manage_default_security_group = true
  default_security_group_tags   = { Name = "${local.name}-default" }

  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = local.tags

}

resource "aws_security_group" "allow_ssh" {
name = "allow-ssh-sg"
vpc_id = module.vpc.vpc_id
ingress {
    cidr_blocks = [
      "0.0.0.0/0"
    ]
from_port = 22
    to_port = 22
    protocol = "tcp"
  }
// Terraform removes the default rule
  egress {
   from_port = 0
   to_port = 0
   protocol = "-1"
   cidr_blocks = ["0.0.0.0/0"]
 }
}
