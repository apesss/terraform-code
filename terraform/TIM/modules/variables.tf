variable "ssh_key" {
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDGL/lSH8aXY1htvNTlc1jYKqLt+ZBJ7z84Jr5cMtLTPfANBeOgsb6qa3xSgixVK6d8q6SdHrNHGGUGXsv/pgFqWVHeSRvIsTfxfsvXODpkS2ZlunQNNQfbTGMhHPrJSj4ZE/HWpV2GygvlcpF6Tmb+cZCAMDcyp8/z1zgLt4Dghq8ute5VAdbQU0c1BUNNGUBmlAnum9hA8B1e9X/Yn+7cUYhx8oiA9BUcEZwWmic4GlYuIe+PFxcjGM4SVTG14KsfiVRH0KBpfd7H9VlsrfpjRGyEr+tEn8lAMsQ3HGOD6mRYMFC0cEIdEFNNhFG6eETJe4EgpOUrfo4i/NHDKy6b4UfnYK4Be56/HpnjjaklYDNJZZcfi2YgLEB815Wl/6cBq63ooTGgGRItxnxb5E6x0oc7cM5ZWPPYUFCPze7nBPO2QnYn7P8WG0IkEB/rCPhe1E8q31Mvr3VY+4IcibW2qRzLkeL2vLKDx6KeDyl6leMrcgqlX5YLduFVgp9J5pk= tilab@terraform-node"
}

variable "proxmox_host" {
    default = "metal3"
}

variable "template_name" {
    default = "ubuntu-2004-cloudinit-template"
}

variable "vm_count" {
    default = 1
}

variable "vm_cores" {
    default = 1
}

variable "vm_mem" {
    default = 2048
}

variable "disk_size" {
    default = "10G"
}

variable "vm_storage" {
    default = "local-lvm"
}

variable "net_bridge" {
    default = "vmbr0"
}
