provider "proxmox" {
  # url is the hostname (FQDN if you have one) for the proxmox host you'd like to connect to to issue the commands. my proxmox host is 'prox-1u'. Add /api2/json at the end for the API
  pm_api_url = "https://163.162.196.66:8006/api2/json"
  # api token id is in the form of: <username>@pam!<tokenId>
  pm_api_token_id = "root@pam!terraform_tocken"
  # this is the full secret wrapped in quotes. don't worry, I've already deleted this from my proxmox cluster by the time you read this post
  pm_api_token_secret = "510ce68d-6e8c-4d35-ba7c-5b6d2d815075"
  # leave tls_insecure set to true unless you have your proxmox SSL certificate situation fully sorted out (if you do, you will know)
  pm_tls_insecure = true
}

module "pvm_module" {
  source = "./modules/proxmox-vm-module"
  vm_mem = "4096"
  vm_cores = 4
}

resource "null_resource" "ansible" {
 count = var.enable_rancher ? 1 : 0
 depends_on = [module.pvm_module]

 provisioner "remote-exec" {
        inline = ["echo 'connected!'"]

        connection {
          type        = "ssh"
          user        = "ubuntu"
          private_key = "${file("~/.ssh/id_rsa")}"
          host = "163.162.196.71"
        }
      }

  provisioner "local-exec" {
        command = " cd ../../ansible && ansible-playbook rancher.yml"
      }
}
